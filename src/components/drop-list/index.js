import React, { Component } from 'react';
 
export default class DropList extends Component {

  generateElements = (arr) => {
    if (arr) {
      let key = 0;
      return arr.map((el) => {
        const template = (
          <li  
            key={key} 
            onMouseOver={() => this.props.handleLiHover(el.objectID)} 
            className={`drop-list__item ${this.props.activeItem === key ? 'active' : ''}`}
            onClick={this.props.handleLiClick}
          >
            {el.locale_names[0]}
          </li>
        );
        key++;
        return template;
      })
    }
      
  };
  
  render() {
    const elements = this.generateElements(this.props.countryList);

    return (
      <ul className="drop-list">
        {elements}
      </ul>
    );
  }
}

