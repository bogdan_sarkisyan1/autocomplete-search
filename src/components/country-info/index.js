import React, { Component } from 'react';
 
export default class CountryInfo extends Component {
  render() {
    
    const {
      info: {
        name,
        population
      }
    } = this.props;

    return (
      <div className="country-info">
        <div className="country-info__item">
          <span className="country-info__title">Name:</span>
          <span className="country-info__content">{name}</span>
        </div>
        <div className="country-info__item">
          <span className="country-info__title">Population:</span>
          <span className="country-info__content">{population} people</span>
        </div>
      </div>
    );
  }
}

