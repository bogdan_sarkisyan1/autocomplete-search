import React, { Component } from 'react';
 
export default class SearchBar extends Component {
  render() {
    
    const {
      handleChange,
      inputValue,
      handleKeyUp,
			handleKeyDown
    } = this.props;

    return (
      <input
        type="text"
        className="search-bar"
        placeholder="Start typing country name"
        onChange={handleChange}
        value={inputValue}
        onKeyUp={handleKeyUp}
				onKeyDown={handleKeyDown}
      />
    );
  }
}

