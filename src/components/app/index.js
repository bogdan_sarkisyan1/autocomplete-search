import React, { Component, Fragment } from 'react';
import SearchBar from '../search-bar';
import countryGet from '../../services/country-service';
import DropList from '../drop-list';
import CountryInfo from '../country-info';

class App extends Component {

  constructor() {
    super();

    this.state = {
      searchValue: '',
      dropListArr: null,
      activeItem: 0,
      countryInfo: null,
      searchFocus: false,
			dropListLoading: false,
    };

  }
  
  componentDidMount() {
  	window.addEventListener('click', this.pageClick);
	}
	
	pageClick = (e) => {
  	if (e.target.closest('.drop-list') || e.target.closest('.search-bar')){
  		this.setState({searchFocus: true});
		} else {
  		this.setState({searchFocus: false});
		}
	};

  onChangeSearch = (e) => {
    this.setState({searchValue: e.target.value, searchFocus: true}, () => {
      this.updateCountriesArray();
    });
  };
	
	onKeyUpSearch = (e) => {
		if (e.keyCode === 13) {
			if (this.state.dropListArr) this.showCountryInfo();
		}
		if (e.keyCode === 27) {
			this.setState({searchValue: ''});
		}
	};
	
	onKeyDownSearch = (e) => {
		if (e.keyCode === 38) {
			if (this.state.dropListArr) this.setState(({activeItem}) => {
				const newVal = activeItem - 1;
				if (activeItem > 0) {
					return {
						activeItem: newVal,
					}
				} else {
					return {
						activeItem: this.state.dropListArr.length - 1,
					}
				}
			});
		}
		if (e.keyCode === 40) {
			if (this.state.dropListArr) this.setState(({activeItem}) => {
				const newVal = activeItem + 1;
				if (activeItem < this.state.dropListArr.length - 1) {
					return {
						activeItem: newVal,
					}
				} else {
					return {
						activeItem: 0,
					}
				}
			});
		}
	};

  handleLiHover = (id) => {
    const index = this.state.dropListArr.findIndex((el) => el.objectID === id);
    this.setState({activeItem: index});
  };
	
  showCountryInfo = () => {
  	const { dropListArr, activeItem } = this.state;
		const countryInfo = {
			population: dropListArr[activeItem].population,
			name: dropListArr[activeItem].locale_names[0],
		};
		
		this.setState({
			countryInfo,
			searchValue: '',
		})
		
	};
  
  updateCountriesArray = () => {
  	const { searchValue } = this.state;
  	
  	if (searchValue) {
  		countryGet(searchValue)
				.then(res => {
					this.setState({
						dropListArr: res,
						activeItem: 0,
					})
				})
		} else {
  		this.setState({dropListArr: null})
		}
	};
	
  render() {
		
    const {
      dropListArr,
      activeItem,
      searchValue,
      countryInfo,
      searchFocus
    } = this.state;
		
    let display;

    if (dropListArr && searchValue && searchFocus) {
      display =
				<DropList
					countryList={dropListArr}
					activeItem={activeItem}
					handleLiHover={this.handleLiHover}
					handleLiClick={() => this.showCountryInfo()}
				/>;
    } else {
      display = null;
    }

    let displayInfo = countryInfo ? <CountryInfo info={countryInfo}/> : <p>Nothing to show</p>;
		
    return (
      <div className={`main-app ${countryInfo ? 'has-info' : ''}`}>
        <SearchBar
					handleChange={this.onChangeSearch}
					inputValue={searchValue}
					handleKeyUp={this.onKeyUpSearch}
					handleKeyDown={this.onKeyDownSearch}
				/>
        {display}
        {displayInfo}
      </div>
    );
  }
}

export default App;
