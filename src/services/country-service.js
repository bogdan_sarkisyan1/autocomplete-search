function countryGet(query) {
  const requestUrl = `https://places-dsn.algolia.net/1/places/query`;
  
  return fetch(requestUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      "query": query,
			"type": "country",
			"language": "en",
    })
  })
    .then(res => res.json())
    .then(res => res.hits)
}

export default countryGet;